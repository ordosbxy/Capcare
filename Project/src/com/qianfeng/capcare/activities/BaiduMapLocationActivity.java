package com.qianfeng.capcare.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.qianfeng.capcare.R;

/**
 * @author 梁桢 <br>
 *         这是一个地图定位功能的类 可以定位用户的当前位置 </br>
 * 
 */
public class BaiduMapLocationActivity extends Activity {
	private MapView mapView;
	// 定义定位服务客户端
	private LocationClient locationClient;
	private BaiduMap baiduMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		// 验证秘钥
		// 传入当前的Application:
		// 注册有name为com.baidu.lbsapi.API_KEY的元数据
		// 百度地图内部将取出对应的value去后台比较正确与否
		SDKInitializer.initialize(getApplicationContext());
		// 装载一个含有MapView的布局文件
		setContentView(R.layout.capcare_activity_location);
		// 获取MapView对象
		mapView = (MapView) findViewById(R.id.mapView);
		baiduMap = mapView.getMap();
		getMyLocation();
		setPointListener();
	}

	private void setPointListener() {
		// 设置标点的监听器
		baiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			@SuppressLint("NewApi")
			@Override
			public boolean onMarkerClick(Marker marker) {
				// 当标点被用户点击时触发该方法

				// 设置弹窗
				// 参数：View,位置,点击弹窗的监听器，Y轴偏移量（-50）
				// 生成一个Button对象，并设置其属性
				Button btn = new Button(getApplicationContext());
				btn.setBackground(getResources().getDrawable(
						R.drawable.capcare_infowindow));
				btn.setTextSize(18);
				btn.setTextColor(Color.WHITE);
				btn.setText("设备 ：西西(32145698 + 12365985)" + "\t\n"
						+ "报警时间 ：2014年6月15日   12:36" + "\t\n"
						+ "报警地点 ：北京市宝盛里天丰利千锋" + "\t\n"
						+ "报警类型 ：超速报警  速度：96km/h");
				// 将Button变成平面的图片（截图设置）
				BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory
						.fromView(btn);
				InfoWindow infoWindow = new InfoWindow(bitmapDescriptor, marker
						.getPosition(), -120, new OnInfoWindowClickListener() {

					@Override
					public void onInfoWindowClick() {
						// 当用户点击弹窗时触发
						// 开始检索
						// 隐藏弹窗
						baiduMap.hideInfoWindow();
					}
				});
				// 显示弹窗对象
				baiduMap.showInfoWindow(infoWindow);
				return false;
			}
		});

	}

	/**
	 * 这是一个获取用户当前 经纬度 并且用图形标记该位置的方法
	 */
	private void getMyLocation() {
		// 定位服务
		// 第一步：初始化接收位置信息的客户端
		locationClient = new LocationClient(getApplicationContext());
		// 第二步：设置定位客户端的参数
		LocationClientOption locationClientOption = new LocationClientOption();
		// 设置坐标类型（bd09ll）
		locationClientOption.setCoorType("bd09ll");
		// 设置扫描间隔
		// locationClientOption.setScanSpan(3000);
		// 设置定位模式（高精度wg/低功耗w/仅设备g）
		locationClientOption.setLocationMode(LocationMode.Hight_Accuracy);
		// 设置是否需要地址信息
		locationClientOption.setIsNeedAddress(true);
		// 第三步：将设置好的参数对象，设置到客户端上
		locationClient.setLocOption(locationClientOption);

		// 第四步：为客户端添加监听器

		locationClient.registerLocationListener(new BDLocationListener() {

			@Override
			public void onReceiveLocation(BDLocation location) {
				// 当接收到位置信息时被触发
				// 1.获取位置 2.用经纬度设置一个点到地图上
				Log.i("",
						"经纬度：" + location.getLatitude() + ","
								+ location.getLongitude());
				LatLng point = new LatLng(location.getLatitude(), location
						.getLongitude());
				BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory
						.fromResource(R.drawable.capcare_alarm_point);

				OverlayOptions overlayOptions = new MarkerOptions().position(
						point).icon(bitmapDescriptor);
				baiduMap.addOverlay(overlayOptions);
				MapStatusUpdate mapStatusUpdate = MapStatusUpdateFactory
						.newLatLng(point);
				baiduMap.animateMapStatus(mapStatusUpdate);

			}
		});

		// 第五步：开启客户端
		locationClient.start();
		// 第六步：开启定位
		if (locationClient.isStarted() && locationClient != null) {
			locationClient.requestLocation();
		}

	}

}
