package com.qianfeng.capcare.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qianfeng.capcare.R;
import com.qianfeng.capcare.utils.DialogUtils;

/**
 * 
 * @author 郭鹏飞
 * @version 1.0
 * @date 14-10-10
 * 
 */
public class VehicleMessageActivity extends Activity implements OnClickListener{

	/**
	 * 头部布局
	 */
	private View view;
	/**
	 * 头部布局的返回
	 */
	private ImageView ivBack;
	/**
	 * 头部布局的确定
	 */
	private TextView tvConfirm;

	/**
	 * 车辆信息设置控件
	 */
	private TextView tvVehicleName, tvVehiclePlateNumber, tvVehicleBrand,
			tvVehicleModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_vehicle_message);

		// 根据id获取到实例对象
		view = findViewById(R.id.capcare_bindingequipment_title);

		ivBack = (ImageView) view
				.findViewById(R.id.capcare_bindingequipment_title_back);
		tvConfirm = (TextView) view
				.findViewById(R.id.capcare_bindingequipment_title_confirm);

		tvVehicleBrand = (TextView) findViewById(R.id.capcare_bindingequipment_vehicle_brand_txt_id);
		tvVehicleModel = (TextView) findViewById(R.id.capcare_bindingequipment_vehicle_model_txt_id);
		tvVehicleName = (TextView) findViewById(R.id.capcare_bindingequipment_vehicle_name_txt_id);
		tvVehiclePlateNumber = (TextView) findViewById(R.id.capcare_bindingequipment_vehicle_plate_number_txt_id);

		// 为控件添加点击事件的监听
		ivBack.setOnClickListener(this);
		tvConfirm.setOnClickListener(this);
		
		tvVehicleBrand.setOnClickListener(this);
		tvVehicleModel.setOnClickListener(this);
		tvVehicleName.setOnClickListener(this);;
		tvVehiclePlateNumber.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.capcare_bindingequipment_title_back:
			// TODO 处理点击返回上一个界面
			finish();
			break;
		case R.id.capcare_bindingequipment_title_confirm:
			// TODO 处理点击确定事件
			Toast.makeText(getApplicationContext(), "确定", 1).show();
			break;
		case R.id.capcare_bindingequipment_vehicle_brand_txt_id:
			DialogUtils.showDialog(this, "车辆品牌", tvVehicleBrand);
			break;
		case R.id.capcare_bindingequipment_vehicle_model_txt_id:
			DialogUtils.showDialog(this, "车辆型号", tvVehicleModel);
			break;
		case R.id.capcare_bindingequipment_vehicle_name_txt_id:
			DialogUtils.showDialog(this, "车辆名称", tvVehicleName);
			break;
		case R.id.capcare_bindingequipment_vehicle_plate_number_txt_id:
			DialogUtils.showDialog(this, "车辆车牌号码", tvVehiclePlateNumber);
			break;
		default:
			break;
		}
	}

}
