package com.qianfeng.capcare.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.qianfeng.capcare.R;

/**
 * 
 * @author 郭鹏飞
 * @version 1.0
 * @date 14-10-10
 * 
 */
public class DialogUtils {

	/**
	 * 显示一个对话框
	 * 
	 * @param context
	 * @param title
	 * @param listener
	 */
	public static void showDialog(final Context context, String title,
			final TextView tvContent) {

		/**
		 * 根据xml文件填充一个布局
		 */
		View view = LayoutInflater.from(context).inflate(
				R.layout.capcare_bindingequipment_other_message_content_edit,
				null);
		/**
		 * 获取该布局中的实例
		 */
		TextView txtTitle = (TextView) view
				.findViewById(R.id.capcare_bindingequipment_other_message_tip_id);
		Button btnCancel = (Button) view
				.findViewById(R.id.capcare_bindingequipment_dialog_cancel_btn_id);
		Button btnConfirm = (Button) view
				.findViewById(R.id.capcare_bindingequipment_dialog_confirm_btn_id);
		txtTitle.setText(title);
		final EditText txtValue = (EditText) view
				.findViewById(R.id.capcare_bindingequipment_other_message_content_et_id);
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		
		/**
		 * 将布局添加到对话框上
		 */
		builder.setView(view);
		
		final AlertDialog dialog = builder.create();
		/**
		 * 取消按钮监听 
		 * 对话框消失
		 */
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		/**
		 * 确定按钮监听
		 * 如果输入正确 设置值 对话框消失
		 * 如果输入出错 Toast 提示 对话框不消失
		 */
		btnConfirm.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String value = txtValue.getText().toString().trim();
				if (TextUtils.isEmpty(value)) {
					Toast.makeText(context, "输入不能为空", 1).show();
				} else {
					tvContent.setText(value);
					dialog.dismiss();
				}
			}
		});

		dialog.show();
	}
}
