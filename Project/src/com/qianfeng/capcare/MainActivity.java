package com.qianfeng.capcare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.qianfeng.capcare.activities.BaiduMapLocationActivity;
import com.qianfeng.capcare.activities.BarCodeTestActivity;
import com.qianfeng.capcare.activities.BindingEquipmentActivity;
import com.qianfeng.capcare.activities.PetMessageActivity;
import com.qianfeng.capcare.activities.UserMessageActivity;
import com.qianfeng.capcare.activities.VehicleMessageActivity;

public class MainActivity extends Activity implements OnClickListener {
	private Button bindEquipment;
	private Button baibuMap;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initViews();

	}

	private void initViews() {
		bindEquipment = (Button) findViewById(R.id.capcate_Bindingequipment);
		bindEquipment.setOnClickListener(this);
		baibuMap = (Button) findViewById(R.id.baiduMap);
		baibuMap.setOnClickListener(this);
	}

	// 测试入口按钮事件
	public void btnBarcodeScanOnClick(View v) {
		Intent intent = new Intent(this, BarCodeTestActivity.class);
		startActivity(intent);
	}

	public void vehicleMessage(View v) {
		Intent intent = new Intent(this, VehicleMessageActivity.class);
		startActivity(intent);
	}

	public void userMessage(View v) {
		Intent intent = new Intent(this, UserMessageActivity.class);
		startActivity(intent);
	}

	public void petMessage(View v) {
		Intent intent = new Intent(this, PetMessageActivity.class);
		startActivity(intent);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.capcate_Bindingequipment:
			// 跳转至设备绑定界面
			startActivity(new Intent(MainActivity.this,
					BindingEquipmentActivity.class));
			break;
		case R.id.baiduMap:
			startActivity(new Intent(MainActivity.this,
					BaiduMapLocationActivity.class));

		default:
			break;
		}

	}

}
